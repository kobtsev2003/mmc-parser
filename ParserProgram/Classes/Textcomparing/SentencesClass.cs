﻿namespace ParserProgram.Classes.Textcomparing
{
    public class SentencesClass
    {
        public int paragraphID;
        public string text;

        public SentencesClass(int paragraphID, string text)
        {
            this.paragraphID = paragraphID;
            this.text = text;
        }
    }
}
